import requests
import re

# __________                                     
# \______   \ ____   _____   ____   ____ ________
#  |       _// __ \ /     \ /  _ \ /    \\___   /
#  |    |   \  ___/|  Y Y  (  <_> )   |  \/    /  By Muhammad Quwais safutra (Remon)
#  |____|_  /\___  >__|_|  /\____/|___|  /_____ \
#         \/     \/      \/            \/      \/

def download_url_files_x(url):
    print "[+] Downloading files.."
    try:
        r = requests.get(url,allow_redirects=True)
        print "[*] Download Complete!"
        save_as = raw_input("[?] Save As  : ")
        print "save as   --> "+save_as
        open(save_as,'wb').write(r.content)
    except:
        print "[!] Failed To Download !"
